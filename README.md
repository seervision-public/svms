# svms - Semantically Versioned Migration Scripts

`svms` is a CLI tool that manages the idempotency of migration scripts for you and ties those scripts together with your releases via [semantic versioning](https://semver.org/).

<!-- toc -->
* [svms - Semantically Versioned Migration Scripts](#svms---semantically-versioned-migration-scripts)
* [Usage](#usage)
* [Commands](#commands)
<!-- tocstop -->

# Usage

<!-- usage -->
```sh-session
$ npm install -g @seervision/svms
$ svms COMMAND
running command...
$ svms (--version)
@seervision/svms/0.6.2 linux-x64 node-v14.19.1
$ svms --help [COMMAND]
USAGE
  $ svms COMMAND
...
```
<!-- usagestop -->

## Configuration

There will be a configuration file generated for you with the first usage of this app. You can find that configuration at `${XDG_CONFIG_HOME}/svms/config.json` (`XDG_CONFIG_HOME` is usually referencing `${HOME}/.config`).

The default configuration can be found [here](https://gitlab.com/seervision-public/svms/-/blob/master/data/config.json).

That configuration file configures:

- a connection to a changelog, where the state about already executed migrations will be stored (only MongoDB connections are supported currently)
- the location of your migration scripts on the filesystem, `svms` will manage their execution for you
- env vars for each migration script, those can be defined separately for each release
- the location of a git repository, in order to scan for release tags like `release/36.0.0`

# Commands

<!-- commands -->
* [`svms add KIND TARGET`](#svms-add-kind-target)
* [`svms autocomplete [SHELL]`](#svms-autocomplete-shell)
* [`svms checkout TARGET`](#svms-checkout-target)
* [`svms help [COMMAND]`](#svms-help-command)
* [`svms push TARGET`](#svms-push-target)
* [`svms status`](#svms-status)

## `svms add KIND TARGET`

Add a new migration script for a semver.

```
USAGE
  $ svms add [KIND] [TARGET] [-d] [-f] [-o] [-v]

ARGUMENTS
  KIND    (js|py) kind of migration script
  TARGET  semver target to add

FLAGS
  -d, --dry      Make any changes as a dry-run, so no changes will be applied.
  -f, --force    Force adding a new migration script (use with care).
  -o, --open     Open the created migration script after file creation with `xdg-open`.
  -v, --verbose  Verbose output to STDOUT.

DESCRIPTION
  Add a new migration script for a semver.

EXAMPLES
  $ svms add js 1.0.1
```

_See code: [dist/commands/add.ts](https://gitlab.com/seervision-public/svms/blob/v0.6.2/dist/commands/add.ts)_

## `svms autocomplete [SHELL]`

display autocomplete installation instructions

```
USAGE
  $ svms autocomplete [SHELL] [-r]

ARGUMENTS
  SHELL  shell type

FLAGS
  -r, --refresh-cache  Refresh cache (ignores displaying instructions)

DESCRIPTION
  display autocomplete installation instructions

EXAMPLES
  $ svms autocomplete

  $ svms autocomplete bash

  $ svms autocomplete zsh

  $ svms autocomplete --refresh-cache
```

_See code: [@oclif/plugin-autocomplete](https://github.com/oclif/plugin-autocomplete/blob/v1.2.0/src/commands/autocomplete/index.ts)_

## `svms checkout TARGET`

Switch to a persistent data state for a release.

```
USAGE
  $ svms checkout [TARGET] [-d] [-f] [-v]

ARGUMENTS
  TARGET  semver target to checkout

FLAGS
  -d, --dry      Make any changes as a dry-run, so no changes will be applied.
  -f, --force    Force a checkout (use with care).
  -v, --verbose  Verbose output to STDOUT.

DESCRIPTION
  Switch to a persistent data state for a release.

EXAMPLES
  $ svms checkout ^1.0.0
```

_See code: [dist/commands/checkout.ts](https://gitlab.com/seervision-public/svms/blob/v0.6.2/dist/commands/checkout.ts)_

## `svms help [COMMAND]`

Display help for svms.

```
USAGE
  $ svms help [COMMAND] [-n]

ARGUMENTS
  COMMAND  Command to show help for.

FLAGS
  -n, --nested-commands  Include all nested commands in the output.

DESCRIPTION
  Display help for svms.
```

_See code: [@oclif/plugin-help](https://github.com/oclif/plugin-help/blob/v5.1.12/src/commands/help.ts)_

## `svms push TARGET`

Push a migration script to the changelog without running it.

```
USAGE
  $ svms push [TARGET] [-d] [-f] [-v]

ARGUMENTS
  TARGET  semver target of migration to push

FLAGS
  -d, --dry      Make any changes as a dry-run, so no changes will be applied.
  -f, --force    Force a push (use with care).
  -v, --verbose  Verbose output to STDOUT.

DESCRIPTION
  Push a migration script to the changelog without running it.

EXAMPLES
  $ svms push 1.0.1
```

_See code: [dist/commands/push.ts](https://gitlab.com/seervision-public/svms/blob/v0.6.2/dist/commands/push.ts)_

## `svms status`

Show the working migration status.

```
USAGE
  $ svms status [-v]

FLAGS
  -v, --verbose  Verbose output to STDOUT.

DESCRIPTION
  Show the working migration status.

EXAMPLES
  $ svms status
```

_See code: [dist/commands/status.ts](https://gitlab.com/seervision-public/svms/blob/v0.6.2/dist/commands/status.ts)_
<!-- commandsstop -->
