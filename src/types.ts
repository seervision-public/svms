export type FilePathAbs = string;
export type Sha256Sum = string;
