import semverGt from "semver/functions/gt";
import semverLt from "semver/functions/lt";
import semverParse from "semver/functions/parse";
import semverSatisfies from "semver/functions/satisfies";
import semverGtr from "semver/ranges/gtr";
import semverLtr from "semver/ranges/ltr";
import semverValidRange from "semver/ranges/valid";

import type { Config } from "../resources";
import { AddArgument } from "./argument";

export interface SemVer {
  major: number;
  minor: number;
  patch: number;
  prerelease?: Array<string | number>;
  build?: Array<string>;
}

type SemVerCore = Pick<SemVer, "major" | "minor" | "patch">;

const SEP_BUILD = "+";
const SEP_IDENTIFIER = ".";
const SEP_PRERELEASE = "-";

/**
 *
 * @param config
 * @returns
 */
export function semVersFromConfig(config: Config): SemVer[] {
  return Object.keys(config.releases)
    .filter((val) => /^(0|([1-9]\d*))$/.test(val))
    .map((val) => ({
      major: Number.parseInt(val, 10),
      minor: 0,
      patch: 0,
    }));
}

/**
 *
 * @param semVer
 * @param option
 * @returns
 */
export function semVerBumped(semVer: SemVer, option: AddArgument): SemVer {
  switch (option) {
    case AddArgument.Major:
      return {
        ...semVer,
        major: semVer.major + 1,
        minor: 0,
        patch: 0,
        prerelease: undefined,
        build: undefined,
      };
    case AddArgument.Minor:
      return {
        ...semVer,
        minor: semVer.minor + 1,
        patch: 0,
        prerelease: undefined,
        build: undefined,
      };
    case AddArgument.Patch:
      return {
        ...semVer,
        patch: semVer.patch + 1,
        prerelease: undefined,
        build: undefined,
      };
    case AddArgument.PreMajor:
      return {
        ...semVer,
        major: semVer.major + 1,
        minor: 0,
        patch: 0,
        prerelease: [0],
        build: undefined,
      };
    case AddArgument.PreMinor:
      return {
        ...semVer,
        minor: semVer.minor + 1,
        patch: 0,
        prerelease: [0],
        build: undefined,
      };
    case AddArgument.PrePatch:
      return {
        ...semVer,
        patch: semVer.patch + 1,
        prerelease: [0],
        build: undefined,
      };
    case AddArgument.PreRelease:
      return {
        ...semVer,
        prerelease: prereleaseBumped(semVer.prerelease),
        build: undefined,
      };
    default:
      ((val: never): never => {
        throw new Error("Cannot bump with option: " + val);
      })(option);
  }
}

/**
 *
 * @param prelease
 * @returns
 */
function prereleaseBumped(
  prelease?: Array<string | number>
): Array<string | number> {
  if (!prelease) {
    return [0];
  }

  const idxNumber = prelease.reduce<number>(
    (acc, val, idx) => (typeof val !== "number" ? acc : idx),
    -1
  );
  if (idxNumber === -1) {
    return [...prelease, 0];
  }

  return prelease.map((val, idx) =>
    idx === idxNumber ? (val as number) + 1 : val
  );
}

/**
 *
 * @param semVerUnparsed
 * @returns
 */
export function semVerParsed(semVerUnparsed: string): SemVer | null {
  if (!isValidSemVer(semVerUnparsed)) {
    return null;
  }

  const [semVerUnparsedA, ...buildUnparsedArr] =
    semVerUnparsed.split(SEP_BUILD);
  const [coreUnparsed, ...prereleaseUnparsedArr] =
    semVerUnparsedA.split(SEP_PRERELEASE);

  const semVer = semverParse(coreUnparsed);
  if (!semVer) {
    return null;
  }

  const prereleaseUnparsed = prereleaseUnparsedArr.join(SEP_PRERELEASE);
  const prerelease = !prereleaseUnparsed
    ? undefined
    : prereleaseUnparsed.split(SEP_IDENTIFIER).map(parsePrereleaseIdentifier);

  const buildUnparsed = buildUnparsedArr.join(SEP_BUILD);
  const build = !buildUnparsed
    ? undefined
    : buildUnparsed.split(SEP_IDENTIFIER);

  return {
    major: semVer.major,
    minor: semVer.minor,
    patch: semVer.patch,
    ...(!prerelease ? {} : { prerelease }),
    ...(!build ? {} : { build }),
  };
}

/**
 *
 * @param semVer
 * @param target
 * @returns
 */
export function isSemVerSatifyingTarget(
  semVer: SemVer,
  target: string
): boolean {
  const label = labelFromSemVer(semVer);
  return semverSatisfies(label, target);
}

/**
 *
 * @param semVer
 * @param target
 * @returns
 */
export function isSemVerGt(semVer: SemVer, target: string): boolean {
  const label = labelFromSemVer(semVer);
  const isRange = semVerParsed(target) === null;
  const compare = isRange ? semverGtr : semverGt;
  return compare(label, target);
}

/**
 *
 * @param semVer
 * @param target
 * @returns
 */
export function isSemVerLt(semVer: SemVer, target: string): boolean {
  const label = labelFromSemVer(semVer);
  const isRange = semVerParsed(target) === null;
  const compare = isRange ? semverLtr : semverLt;
  return compare(label, target);
}

/**
 *
 * @param target
 * @returns
 */
export function isValidSemVerTarget(target: string): boolean {
  return semverValidRange(target) !== null;
}

/**
 *
 * @param semVer
 * @returns
 */
export function labelFromSemVer(semVer: SemVer): string {
  const core = [semVer.major, semVer.minor, semVer.patch].join(SEP_IDENTIFIER);
  const prerelease = (semVer.prerelease || [])
    .reduce<string>((acc, val) => acc + val + SEP_IDENTIFIER, SEP_PRERELEASE)
    .slice(0, -1);
  const build = (semVer.build || [])
    .reduce((acc, val) => acc + val + SEP_IDENTIFIER, SEP_BUILD)
    .slice(0, -1);

  return core + prerelease + build;
}

/**
 *
 * @param a
 * @param b
 * @returns
 */
export function sortAscSemVer(a: SemVer, b: SemVer): number {
  if (hasEqualSemVerPrecedence(a, b)) {
    return 0;
  }

  const bLabel = labelFromSemVer(b); // TODO: find a more elegant solution
  return isSemVerGt(a, bLabel) ? 1 : -1;
}

/**
 *
 * @param a
 * @param b
 * @returns
 */
export function sortDescSemVer(a: SemVer, b: SemVer): number {
  let result = sortAscSemVer(a, b);
  if (result !== 0) {
    result *= -1;
  }
  return result;
}

/**
 *
 * @param a
 * @param b
 * @returns
 */
export function hasEqualSemVerPrecedence(a: SemVer, b: SemVer): boolean {
  const { prerelease: aPrerelease, ...aSemVerCore } = a;
  const { prerelease: bPrerelease, ...bSemVerCore } = b;
  if (!hasEqualSemVerCore(aSemVerCore, bSemVerCore)) {
    return false;
  }

  const aPrereleaseFixed = aPrerelease || [];
  const bPrereleaseFixed = bPrerelease || [];
  if (!aPrereleaseFixed.length) {
    return !bPrereleaseFixed.length;
  }

  return aPrereleaseFixed.reduce<boolean>(
    (acc, val, idx) => acc && val === bPrereleaseFixed[idx],
    true
  );
}

/**
 *
 * @param a
 * @param b
 * @returns
 */
function hasEqualSemVerCore(a: SemVerCore, b: SemVerCore): boolean {
  return a.major === b.major && a.minor === b.minor && a.patch === b.patch;
}

/**
 * See spec: https://semver.org/#backusnaur-form-grammar-for-valid-semver-versions
 * See ref: https://ihateregex.io/expr/semver/
 *
 * @param value
 * @returns
 */
function isValidSemVer(value?: string): boolean {
  if (!value) {
    return false;
  }

  return /^(0|[1-9]\d*)\.(0|[1-9]\d*)\.(0|[1-9]\d*)(?:-((?:0|[1-9]\d*|\d*[A-Za-z-][\dA-Za-z-]*)(?:\.(?:0|[1-9]\d*|\d*[A-Za-z-][\dA-Za-z-]*))*))?(?:\+([\dA-Za-z-]+(?:\.[\dA-Za-z-]+)*))?$/.test(
    value
  );
}

/**
 *
 * @param value
 * @returns
 */
function parsePrereleaseIdentifier(value: string): string | number {
  const isNumeric = /^(0|[1-9]\d*)$/.test(value);
  if (!isNumeric) {
    return value;
  }

  return Number.parseInt(value, 10);
}
