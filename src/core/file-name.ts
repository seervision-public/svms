import { SemVer, labelFromSemVer, semVerParsed } from "./sem-ver";

/**
 * The basename of a migration-script file.
 * It represents the SemVer information that is used to determine the SemVer precendence (major,
 * minor, patch and pre-release).
 */
type FileName = string;

/**
 *
 * @param semVer
 * @returns
 */
export function fileNameFromSemVer(semVer: SemVer): FileName {
  const { build, ...semVerPrecedented } = semVer;
  const label = labelFromSemVer(semVerPrecedented);
  return label.replace(/\./g, "_");
}

/**
 *
 * @param fileName
 */
export function semVerFromFileName(fileName: FileName): SemVer {
  const label = fileName.replace(/_/g, ".");
  const semVer = semVerParsed(label);
  if (!semVer) {
    throw new Error("Cannot parse SemVer from filename: " + fileName);
  }

  const { build, ...semVerPrecedented } = semVer;
  return semVerPrecedented;
}
