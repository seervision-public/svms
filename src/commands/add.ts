import cp from "child_process";
import fsPromises from "fs/promises";

import { Command, Flags } from "@oclif/core";

import * as core from "../core";
import { MigrationScriptKind } from "../resources";

export default class Add extends Command {
  static description = "Add a new migration script for a semver.";

  static examples = ["<%= config.bin %> <%= command.id %> js 1.0.1"];

  static flags = {
    dry: Flags.boolean({
      char: "d",
      default: false,
      description:
        "Make any changes as a dry-run, so no changes will be applied.",
    }),
    force: Flags.boolean({
      char: "f",
      default: false,
      description: "Force adding a new migration script (use with care).",
    }),
    open: Flags.boolean({
      char: "o",
      default: false,
      description:
        "Open the created migration script after file creation with `xdg-open`.",
    }),
    verbose: Flags.boolean({
      char: "v",
      default: false,
      description: "Verbose output to STDOUT.",
    }),
  };

  static args = [
    {
      name: "kind",
      required: true,
      description: "kind of migration script",
      options: Object.values(MigrationScriptKind),
    },
    {
      name: "target",
      required: true,
      description: "semver target to add",
    },
  ];

  public async run(): Promise<void> {
    const { args, flags } = await this.parse(Add);
    const kind = args.kind as MigrationScriptKind;
    const targetRaw = args.target;
    const dry = flags.dry;
    const force = flags.force;
    const open = flags.open;
    const verbose = flags.verbose;

    if (verbose) {
      this.log("Creating API...");
    }
    const api = await core.createApi(this.config.configDir);

    // construct semver target
    // bump version wrt the latest git release tag
    let target: core.SemVer | null = null;
    if (Object.values(core.AddArgument).includes(targetRaw)) {
      const migItemTagged = api.items.find(core.isMigItemTagged);
      if (!migItemTagged) {
        this.error(
          "No migration to bump. Please create a distinct version first.",
          { exit: 1 }
        );
      }
      target = core.semVerBumped(migItemTagged.semVer, targetRaw);
    } else {
      target = core.semVerParsed(targetRaw);
      if (!target) {
        this.error("Cannot parse target to semver: " + targetRaw, { exit: 1 });
      }
    }

    // ensure target is properly configured
    if (!api.hasConfiguration(target)) {
      if (!dry && open) {
        cp.spawn(`xdg-open ${this.config.configDir}`);
      }
      this.error(
        "Release not known for semver: " + core.labelFromSemVer(target),
        {
          exit: 1,
        }
      );
    }

    // ensure uniqueness of semver
    const migrationFound = api.items
      .filter(core.isMigItemAvailable)
      .find(core.isMigItemMatchingSemVer(target));
    if (migrationFound) {
      if (force) {
        if (!dry) {
          await fsPromises.rm(migrationFound.migration.filePath);
        }
      } else {
        if (!dry && open) {
          const filePathExisting = migrationFound.migration.filePath;
          cp.spawn(`xdg-open ${filePathExisting}`);
        }
        this.error(
          "Migration already exists for semver: " +
            core.labelFromSemVer(target),
          {
            exit: 1,
          }
        );
      }
    }

    if (!dry) {
      const filePathCreated = await api.create(target, kind);
      if (open) {
        cp.spawn(`xdg-open ${filePathCreated}`);
      }
    }

    this.exit();
  }
}
