import path from "path";

import fse from "fs-extra";

import { SemVer, semVerParsed } from "../core";
import { FilePathAbs } from "../types";

/**
 *
 * @returns
 */
export function dataDir(): FilePathAbs {
  return path.resolve(path.join(__dirname, "..", "..", "data"));
}

/**
 *
 * @returns
 */
export async function readPkgSemVer(): Promise<SemVer> {
  const filepath = path.resolve(
    path.join(__dirname, "..", "..", "package.json")
  );
  const pkgJson = await fse.readJson(filepath);
  const semVer = semVerParsed(pkgJson.version);
  if (!semVer) {
    throw new Error("Cannot get package version");
  }

  return semVer;
}
