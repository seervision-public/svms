import { expect } from "chai";

import { SemVer, labelFromSemVer } from "../../src/core/sem-ver";

describe("sem-ver", () => {
  describe("labelFromSemVer", () => {
    it("creates label correctly", () => {
      const semVer: SemVer = {
        major: 31,
        minor: 2,
        patch: 300,
      };

      const actual = labelFromSemVer(semVer);
      const expected = "31.2.300";

      expect(actual).to.equal(expected);
    });
  });
});
