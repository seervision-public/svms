import { expect } from "chai";

import {
  MigItem,
  MigItemKind,
  isMigItemSatifyingTarget,
  migItemsFromResources,
  orderForApplying,
  orderForReverting,
} from "../../src/core/mig-item";
import { MigrationScriptKind } from "../../src/resources/migration-script";

describe("mig-item", () => {
  describe("migItemsFromResources", () => {
    it("can handle empty state", () => {
      const actual = migItemsFromResources([], [], [], []);
      const expected: MigItem[] = [];

      expect(actual).to.eql(expected);
    });

    it("returns order for reversal", () => {
      const semVersConfigured = [
        {
          major: 30,
          minor: 0,
          patch: 0,
        },
        {
          major: 31,
          minor: 0,
          patch: 0,
        },
        {
          major: 34,
          minor: 0,
          patch: 0,
        },
        {
          major: 33,
          minor: 0,
          patch: 0,
        },
        {
          major: 32,
          minor: 0,
          patch: 0,
        },
      ];
      const releaseTags = [
        "30.0.0",
        "30.0.1",
        "31.1.0",
        "30.1.0",
        "30.1.1",
        "31.0.0",
        "29",
      ];
      const migrationsApplied = [
        {
          appliedAt: new Date(0),
          semVer: {
            major: 30,
            minor: 0,
            patch: 0,
          },
          sha256sum: "myshasum-30.0.0",
        },
      ];
      const migrationsAvailable = [
        {
          filePath: "/home/user/script-30.0.0.js",
          kind: MigrationScriptKind.Node,
          semVer: {
            major: 30,
            minor: 0,
            patch: 0,
          },
          sha256sum: "myshasum-30.0.0",
        },
        {
          filePath: "/home/user/script-30.0.1.js",
          kind: MigrationScriptKind.Node,
          semVer: {
            major: 30,
            minor: 0,
            patch: 1,
          },
          sha256sum: "myshasum-30.0.1",
        },
      ];

      const actual = migItemsFromResources(
        semVersConfigured,
        releaseTags,
        migrationsApplied,
        migrationsAvailable
      );
      const expected: MigItem[] = [
        {
          kind: MigItemKind.___,
          semVer: {
            major: 34,
            minor: 0,
            patch: 0,
          },
        },
        {
          kind: MigItemKind.___,
          semVer: {
            major: 33,
            minor: 0,
            patch: 0,
          },
        },
        {
          kind: MigItemKind.___,
          semVer: {
            major: 32,
            minor: 0,
            patch: 0,
          },
        },
        {
          kind: MigItemKind.T__,
          semVer: {
            major: 31,
            minor: 1,
            patch: 0,
          },
          tag: "31.1.0",
        },
        {
          kind: MigItemKind.T__,
          semVer: {
            major: 31,
            minor: 0,
            patch: 0,
          },
          tag: "31.0.0",
        },
        {
          kind: MigItemKind.T__,
          semVer: {
            major: 30,
            minor: 1,
            patch: 1,
          },
          tag: "30.1.1",
        },
        {
          kind: MigItemKind.T__,
          semVer: {
            major: 30,
            minor: 1,
            patch: 0,
          },
          tag: "30.1.0",
        },
        {
          kind: MigItemKind.TM_,
          migration: {
            filePath: "/home/user/script-30.0.1.js",
            kind: MigrationScriptKind.Node,
            semVer: {
              major: 30,
              minor: 0,
              patch: 1,
            },
            sha256sum: "myshasum-30.0.1",
          },
          semVer: {
            major: 30,
            minor: 0,
            patch: 1,
          },
          tag: "30.0.1",
        },
        {
          kind: MigItemKind.TMA,
          migration: {
            appliedAt: new Date(0),
            filePath: "/home/user/script-30.0.0.js",
            kind: MigrationScriptKind.Node,
            semVer: {
              major: 30,
              minor: 0,
              patch: 0,
            },
            sha256sum: "myshasum-30.0.0",
          },
          semVer: {
            major: 30,
            minor: 0,
            patch: 0,
          },
          tag: "30.0.0",
        },
      ];

      expect(actual).to.eql(expected);
    });
  });

  describe("isMigItemSatifyingTarget", () => {
    it("finds correct target with ^", () => {
      const items: MigItem[] = [
        {
          kind: MigItemKind.___,
          semVer: {
            major: 34,
            minor: 0,
            patch: 0,
          },
        },
        {
          kind: MigItemKind.___,
          semVer: {
            major: 33,
            minor: 0,
            patch: 0,
          },
        },
        {
          kind: MigItemKind.___,
          semVer: {
            major: 32,
            minor: 0,
            patch: 0,
          },
        },
        {
          kind: MigItemKind.T__,
          semVer: {
            major: 31,
            minor: 1,
            patch: 0,
          },
          tag: "31.1.0",
        },
        {
          kind: MigItemKind.T__,
          semVer: {
            major: 31,
            minor: 0,
            patch: 0,
          },
          tag: "31.0.0",
        },
        {
          kind: MigItemKind.T__,
          semVer: {
            major: 30,
            minor: 1,
            patch: 1,
          },
          tag: "30.1.1",
        },
        {
          kind: MigItemKind.T__,
          semVer: {
            major: 30,
            minor: 1,
            patch: 0,
          },
          tag: "30.1.0",
        },
        {
          kind: MigItemKind.TM_,
          migration: {
            filePath: "/home/user/script-30.0.1.js",
            kind: MigrationScriptKind.Node,
            semVer: {
              major: 30,
              minor: 0,
              patch: 1,
            },
            sha256sum: "myshasum-30.0.1",
          },
          semVer: {
            major: 30,
            minor: 0,
            patch: 1,
          },
          tag: "30.0.1",
        },
        {
          kind: MigItemKind.TMA,
          migration: {
            appliedAt: new Date(0),
            filePath: "/home/user/script-30.0.0.js",
            kind: MigrationScriptKind.Node,
            semVer: {
              major: 30,
              minor: 0,
              patch: 0,
            },
            sha256sum: "myshasum-30.0.0",
          },
          semVer: {
            major: 30,
            minor: 0,
            patch: 0,
          },
          tag: "30.0.0",
        },
      ];
      const target = "^30.1";

      const actual = items.find(isMigItemSatifyingTarget(target));
      const expected = {
        kind: MigItemKind.T__,
        semVer: {
          major: 30,
          minor: 1,
          patch: 1,
        },
        tag: "30.1.1",
      };

      expect(actual).to.eql(expected);
    });

    it("finds correct target with exact version", () => {
      const items: MigItem[] = [
        {
          kind: MigItemKind.___,
          semVer: {
            major: 34,
            minor: 0,
            patch: 0,
          },
        },
        {
          kind: MigItemKind.___,
          semVer: {
            major: 33,
            minor: 0,
            patch: 0,
          },
        },
        {
          kind: MigItemKind.___,
          semVer: {
            major: 32,
            minor: 0,
            patch: 0,
          },
        },
        {
          kind: MigItemKind.T__,
          semVer: {
            major: 31,
            minor: 1,
            patch: 0,
          },
          tag: "31.1.0",
        },
        {
          kind: MigItemKind.T__,
          semVer: {
            major: 31,
            minor: 0,
            patch: 0,
          },
          tag: "31.0.0",
        },
        {
          kind: MigItemKind.T__,
          semVer: {
            major: 30,
            minor: 1,
            patch: 1,
          },
          tag: "30.1.1",
        },
        {
          kind: MigItemKind.T__,
          semVer: {
            major: 30,
            minor: 1,
            patch: 0,
          },
          tag: "30.1.0",
        },
        {
          kind: MigItemKind.TM_,
          migration: {
            filePath: "/home/user/script-30.0.1.js",
            kind: MigrationScriptKind.Node,
            semVer: {
              major: 30,
              minor: 0,
              patch: 1,
            },
            sha256sum: "myshasum-30.0.1",
          },
          semVer: {
            major: 30,
            minor: 0,
            patch: 1,
          },
          tag: "30.0.1",
        },
        {
          kind: MigItemKind.TMA,
          migration: {
            appliedAt: new Date(0),
            filePath: "/home/user/script-30.0.0.js",
            kind: MigrationScriptKind.Node,
            semVer: {
              major: 30,
              minor: 0,
              patch: 0,
            },
            sha256sum: "myshasum-30.0.0",
          },
          semVer: {
            major: 30,
            minor: 0,
            patch: 0,
          },
          tag: "30.0.0",
        },
      ];
      const target = "30.1.0";

      const actual = items.find(isMigItemSatifyingTarget(target));
      const expected = {
        kind: MigItemKind.T__,
        semVer: {
          major: 30,
          minor: 1,
          patch: 0,
        },
        tag: "30.1.0",
      };

      expect(actual).to.eql(expected);
    });
  });

  describe("orderForApplying", () => {
    const semVer = {
      major: 30,
      minor: 0,
      patch: 0,
    };

    const M_: MigItem = {
      kind: MigItemKind._M_,
      migration: {
        filePath: "/home/user/script.js",
        kind: MigrationScriptKind.Node,
        semVer,
        sha256sum: "myshasum",
      },
      semVer,
    };
    const _A: MigItem = {
      kind: MigItemKind.__A,
      migration: {
        appliedAt: new Date(0),
        semVer,
        sha256sum: "myshasum",
      },
      semVer,
    };
    const MA: MigItem = {
      kind: MigItemKind._MA,
      migration: {
        appliedAt: new Date(0),
        filePath: "/home/user/script.js",
        kind: MigrationScriptKind.Node,
        semVer,
        sha256sum: "myshasum",
      },
      semVer,
    };

    describe("handles conflicting SemVers", () => {
      it("M_ / _A", () => {
        const a = M_;
        const b = _A;

        const actual = [a, b].sort(orderForApplying);
        const expected = [a, b];

        expect(actual).to.eql(expected);
      });

      it("M_ / MA", () => {
        const a = M_;
        const b = MA;

        const actual = [a, b].sort(orderForApplying);
        const expected = [a, b];

        expect(actual).to.eql(expected);
      });

      it("_A / MA", () => {
        const a = _A;
        const b = MA;

        const actual = [a, b].sort(orderForApplying);
        const expected = [a, b];

        expect(actual).to.eql(expected);
      });

      it("MA / _A", () => {
        const a = MA;
        const b = _A;

        const actual = [a, b].sort(orderForApplying);
        const expected = [b, a];

        expect(actual).to.eql(expected);
      });

      it("MA / M_", () => {
        const a = MA;
        const b = M_;

        const actual = [a, b].sort(orderForApplying);
        const expected = [b, a];

        expect(actual).to.eql(expected);
      });

      it("_A / M_", () => {
        const a = _A;
        const b = M_;

        const actual = [a, b].sort(orderForApplying);
        const expected = [b, a];

        expect(actual).to.eql(expected);
      });
    });

    describe("handles non-conflicting Semvers", () => {
      it("M_ / M_", () => {
        const semVerA = {
          ...M_.semVer,
          patch: 1,
        };
        const semVerB = {
          ...M_.semVer,
          patch: 2,
        };

        const a = {
          ...M_,
          migration: {
            ...M_.migration,
            semVer: semVerA,
          },
          semVer: semVerA,
        };
        const b = {
          ...M_,
          migration: {
            ...M_.migration,
            semVer: semVerB,
          },
          semVer: semVerB,
        };

        const actual = [a, b].sort(orderForApplying);
        const expected = [a, b];

        expect(actual).to.eql(expected);
      });
    });
  });

  describe("orderForReverting", () => {
    const semVer = {
      major: 30,
      minor: 0,
      patch: 0,
    };

    const M_: MigItem = {
      kind: MigItemKind._M_,
      migration: {
        filePath: "/home/user/script.js",
        kind: MigrationScriptKind.Node,
        semVer,
        sha256sum: "myshasum",
      },
      semVer,
    };
    const _A: MigItem = {
      kind: MigItemKind.__A,
      migration: {
        appliedAt: new Date(0),
        semVer,
        sha256sum: "myshasum",
      },
      semVer,
    };
    const MA: MigItem = {
      kind: MigItemKind._MA,
      migration: {
        appliedAt: new Date(0),
        filePath: "/home/user/script.js",
        kind: MigrationScriptKind.Node,
        semVer,
        sha256sum: "myshasum",
      },
      semVer,
    };

    describe("handles conflicting SemVers", () => {
      it("M_ / _A", () => {
        const a = M_;
        const b = _A;

        const actual = [a, b].sort(orderForReverting);
        const expected = [b, a];

        expect(actual).to.eql(expected);
      });

      it("M_ / MA", () => {
        const a = M_;
        const b = MA;

        const actual = [a, b].sort(orderForReverting);
        const expected = [b, a];

        expect(actual).to.eql(expected);
      });

      it("_A / MA", () => {
        const a = _A;
        const b = MA;

        const actual = [a, b].sort(orderForReverting);
        const expected = [b, a];

        expect(actual).to.eql(expected);
      });

      it("MA / _A", () => {
        const a = MA;
        const b = _A;

        const actual = [a, b].sort(orderForReverting);
        const expected = [a, b];

        expect(actual).to.eql(expected);
      });

      it("MA / M_", () => {
        const a = MA;
        const b = M_;

        const actual = [a, b].sort(orderForReverting);
        const expected = [a, b];

        expect(actual).to.eql(expected);
      });

      it("_A / M_", () => {
        const a = _A;
        const b = M_;

        const actual = [a, b].sort(orderForReverting);
        const expected = [a, b];

        expect(actual).to.eql(expected);
      });
    });

    describe("handles non-conflicting Semvers", () => {
      it("MA / MA", () => {
        const semVerA = {
          ...MA.semVer,
          patch: 1,
        };
        const semVerB = {
          ...MA.semVer,
          patch: 2,
        };

        const a = {
          ...MA,
          migration: {
            ...MA.migration,
            semVer: semVerA,
          },
          semVer: semVerA,
        };
        const b = {
          ...MA,
          migration: {
            ...MA.migration,
            semVer: semVerB,
          },
          semVer: semVerB,
        };

        const actual = [a, b].sort(orderForReverting);
        const expected = [b, a];

        expect(actual).to.eql(expected);
      });
    });
  });
});
